#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

bool isNumber(char *str){
    while(*str != '\0') {
        if(isdigit(*str) == false) {
            return false;
        }
        str++;
    }
    return true;
}

enum METHOD {
    ADD,
    SUB,
    MULT,
    DIV,
    INVALID
};

int main(int argc, char *argv[])
{
	if (argc < 3)
	{
		printf("error There are less than 2 params\n");
		return -1;
	}

    int value1 = -1, value2 = -1;
    METHOD method = METHOD::INVALID;
    for(int i = 1; i < argc; i++){
        if(argv[i][0] == '/' && (argv[i][2] == ' ' || argv[i][2] == '\0')) {
            switch(argv[i][1]) {
                case 'a':
                    method = METHOD::ADD;
                    break;
                case 's':
                    method = METHOD::SUB;
                    break;
                case 'm':
                    method = METHOD::MULT;
                    break;
                case 'd':
                    method = METHOD::DIV;
                    break;
                default:
                    printf("Invalid Switch\n");
                    return -3;
            }
        } else if(isNumber(argv[i]) == true) {
            if(value1 == -1) {
                value1 = atoi(argv[i]);
            } else if(value2 == -1) {
                value2 = atoi(argv[i]);
            } else {
                printf("Too many input values\n");
                return -2;
            }
        } else {
            printf("Invalid paramter\n");
            return -3;
        }
    }

    if(method == METHOD::INVALID) {
    }
    if(value1 == -1 || value2 == -1) {
        printf("Need 2 number parameters\n");
        return -5;
    }

    switch(method) {
        case METHOD::ADD:
	        printf("%i\n", value1 + value2);
            break;
        case METHOD::SUB:
	        printf("%i\n", value1 - value2);
            break;
        case METHOD::MULT:
	        printf("%i\n", value1 * value2);
            break;
        case METHOD::DIV:
	        printf("%i\n", value1 / value2);
            break;
        default:
            printf("Method not set");
            return -4;
    }
}